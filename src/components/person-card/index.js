import React, { Component } from 'react';
import './index.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart, faTimes } from '@fortawesome/free-solid-svg-icons'
import { Widget, addResponseMessage } from 'react-chat-widget';
import ChatApp from './../chat-app/ChatApp'


import 'react-chat-widget/lib/styles.css';

export class PersonCard extends Component {
  constructor() {
    super()
    
    library.add(faHeart, faTimes)

    this.handleLove = this.handleLove.bind(this);
    this.handleHate = this.handleHate.bind(this);

    // Bind 'this' to event handlers. React ES6 does not do this by default
    this.usernameChangeHandler = this.usernameChangeHandler.bind(this);
    this.usernameSubmitHandler = this.usernameSubmitHandler.bind(this);

    this.state = {
        love:false
    }
  }

  usernameChangeHandler(event) {
    this.setState({ username: event.target.value });
  }

  usernameSubmitHandler(event) {
    event.preventDefault();
    this.setState({ submitted: true, username: this.state.username });
  }

  componentDidMount() {
    addResponseMessage("Welcome to this awesome chat!");
  }

  handleNewUserMessage = (newMessage) => {
    console.log(`New message incomig! ${newMessage}`);
    // Now send the message throught the backend API
    addResponseMessage(newMessage);
  }

  handleHate(event, id, name) {
    this.setState({
        love: false,
        id,
        name
    })

    this.props.deleteCallback(id);
  }

  handleLove(event, id, name) {
    this.setState({
        love: true,
        id,
        name
    });
    this.usernameChangeHandler({target: {value: name}});
    this.props.loveCallback(id, name);
  }
  
  render() {
    const { id, imgUrl, name, description } = this.props.data;
    console.log("the image is : ", this.props.data) 

    return (
        <div className="container">
            <section id="fake-tinder">
                <div className="cards">
                    <div className="card one" id="card-one" style={{backgroundImage: `url(${imgUrl})`}}>
                        <div className="buttons">
                            <div className="button no">
                                <i className="fa fa-times" aria-hidden="true" onClick={(e) => this.handleHate(e, id, name)}>
                                    <FontAwesomeIcon icon="times"/>
                                </i>
                            </div>
                            <div className="button yes">
                                <i className="fa fa-heart" aria-hidden="true" onClick={(e) => this.handleLove(e, id, name)}>
                                    <FontAwesomeIcon icon="heart"/>
                                </i>
                            </div>
                        </div>
                        <div className="info">
                            <h2 style={{textAlign: "left", marginBottom: "0.2em"}}>{name}</h2>
                            <p style={{textAlign: "left"}}>{description}</p>
                        </div>
                    </div>
                </div>
            </section>
            <Widget handleNewUserMessage={this.handleNewUserMessage}/>
        </div>
    )}
}