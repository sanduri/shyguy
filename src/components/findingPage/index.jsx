import React, { Component } from 'react';
import logo from './../../logo.svg';
import './index.css';
import { NoOneAround } from './../no-one'
import { HamburgerButton } from './../hamburger'
import { PersonCard } from './../person-card'
import ChatApp from './../chat-app/ChatApp'

var rp = require('request-promise');

export default class FindingPage extends Component {

  constructor() {
    super()
    
    this.state = {
      waiting: true,
      errors: {
        isError: false,
        errors: {
        }
      },
      chat: {
        isOpened: false,
        id: null, 
        name: ""
      },
      peopleAround: []
    }

    this.deleteCard = this.deleteCard.bind(this);
    this.searchPeople = this.searchPeople.bind(this);
    this.lovePressed = this.lovePressed.bind(this);
  }

  lovePressed(id, name) {
    const newUsersList = this.state.peopleAround.users.filter((obj) => obj.id == id);

    this.setState({
      peopleAround: newUsersList,
      chat: {
        isOpened: true,
        id, 
        name
      }
    })
  }

  //function that gets the location and returns it
  getLocation() {
    if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.showPosition);
    } else {
      console.log("Geo Location not supported by browser");
    }
  }

  //function that retrieves the position
  showPosition(position) {
    var location = {
      longitude: position.coords.longitude,
      latitude: position.coords.latitude
    }
    console.log(location)
  }


  async componentDidMount() {
    this.getLocation();

    this.setState({
      waiting: true
    });

    const peopleAround = await this.searchPeople();

    const myObj = {
      "users": [
        {"id": 1, "imgUrl": "http://www.soldius.com/file/2018/01/hiring_great_lawyers.jpg","name": "yotama","description": "Thats my name!"},
        {"id": 2, "imgUrl": "https://images.unsplash.com/photo-1433588641602-7c1083c4f0e2","name": "dana","description": "Thats my name!"},
        {"id": 3, "imgUrl": "https://scontent.xx.fbcdn.net/v/t1.0-1/c0.0.160.160/p160x160/15027751_10211072479511386_2026968575619531763_n.jpg?_nc_cat=0&oh=eab7ebbe5c4a5d1c091c940dcdb00642&oe=5C19A02C","name": "shelly","description": "Thats my name!"},
        {"id": 4, "imgUrl": "https://vignette.wikia.nocookie.net/king-harkinian/images/4/47/Spongebob.png/revision/latest/scale-to-width-down/250?cb=20170518185222","name": "nichole","description": "Thats my name!"},
        {"id": 5, "imgUrl": "https://static1.squarespace.com/static/544a89a9e4b03e16957adb96/58cd7cc5bf629a7051c749de/58cd7d1cc534a59fbdb49802/1489861918883/Shredder-Print_8x10_sm.png?format=500w","name": "nice one","description": "Thats my name!"},
        {"id": 6, "imgUrl": "https://www.cerebralpalsyguidance.com/wp-content/uploads/2016/01/lawyer.jpg","name": "another1","description": "Thats my name!"},
        {"id": 7, "imgUrl": "https://www.scarymommy.com/wp-content/uploads/2015/07/scary-grandma-featured.jpg?w=700","name": "some1","description": "Thats my name!"},
        {"id": 8, "imgUrl": "http://nextluxury.com/wp-content/uploads/cool-nice-beards-for-men.jpg","name": "sample 1","description": "Thats my name!"},
        {"id": 9, "imgUrl": "http://blog.antics.com/wp-content/uploads/2015/07/grandma_1800x1800-300x300.jpg","name": "granma","description": "Thats my name!"},
        {"id": 10, "imgUrl": "https://img1.liveinternet.ru/images/attach/d/0/140/22/140022479_3416556_9f6a2020132c5a78ee7644eac43ccb6d.jpg","name": "Damn girl","description": "Thats my name!"},
        {"id": 11, "imgUrl": "https://images.unsplash.com/photo-1433588641602-7c1083c4f0e2","name": "just a girl","description": "Thats my name!"},
        {"id": 12, "imgUrl": "https://scontent.xx.fbcdn.net/v/t1.0-1/c0.0.160.160/p160x160/15027751_10211072479511386_2026968575619531763_n.jpg?_nc_cat=0&oh=eab7ebbe5c4a5d1c091c940dcdb00642&oe=5C19A02C","name": "another 1","description": "Thats my name!"},
        {"id": 12, "imgUrl": "https://scontent.xx.fbcdn.net/v/t1.0-1/c0.0.160.160/p160x160/15027751_10211072479511386_2026968575619531763_n.jpg?_nc_cat=0&oh=eab7ebbe5c4a5d1c091c940dcdb00642&oe=5C19A02C","name": "another 1","description": "Thats my name!"},
        {"id": 12, "imgUrl": "https://scontent.xx.fbcdn.net/v/t1.0-1/c0.0.160.160/p160x160/15027751_10211072479511386_2026968575619531763_n.jpg?_nc_cat=0&oh=eab7ebbe5c4a5d1c091c940dcdb00642&oe=5C19A02C","name": "another 1","description": "Thats my name!"},
        {"id": 12, "imgUrl": "https://scontent.xx.fbcdn.net/v/t1.0-1/c0.0.160.160/p160x160/15027751_10211072479511386_2026968575619531763_n.jpg?_nc_cat=0&oh=eab7ebbe5c4a5d1c091c940dcdb00642&oe=5C19A02C","name": "another 1","description": "Thats my name!"}
      ]
    } 

    setTimeout(() => {
      this.setState({
        peopleAround: myObj,
        waiting: false
      });
    }, 2200);

  }
  
  deleteCard(id) {
    const newUsersList = this.state.peopleAround.users.filter((obj) => obj.id !== id);
    
    this.setState({
      peopleAround: {
        users: newUsersList
      }
    })
  }

  async searchPeople() {
    const endpoint = `${process.env.REACT_APP_SERVER_URL}/api/search`;
    console.log(`trying to fetch ${endpoint}`);
    var options = {
      method: 'GET',
      uri: endpoint,
      qs: {
        "location": this.state.location
      }
    };
    
    rp(options)
      .then((data) => {
          console.log(data);
          return data;
      })
      .catch((err) => {
          console.log("failed to fetch...")
      });

    return {"error": "failed"};
  }

  render() {
  console.log("stateeeee: ", this.state)
    return (
      <div className="App">
        <div className="App-header">
          <HamburgerButton />
          <h1 className="App-title">Hi mister shy guy!</h1>
        </div>

        {
          this.state.errors.isError && (
            <div>
              <p>Oops, looks like you're going to sleep alone tonight... SORRY!!</p>
              <img style={{width: '100%'}} src='/error.png'></img>
            </div>
          )
        }

        { 
          !this.state.errors.isError && this.state.waiting && <NoOneAround/> 
        }

        <div name="personList">
        {
          console.log("arounddd", this.state.peopleAround)
        } {
          !this.state.errors.isError && !this.state.waiting && this.state.peopleAround && this.state.peopleAround.users ? (
            this.state.peopleAround.users.map(curr => {
              console.log(curr)
              return (
                <div>
                  <PersonCard key={curr.id} data={curr} loveCallback={this.lovePressed} deleteCallback={this.deleteCard}/> 
                </div>
              );
            })
          ) : null
        }
          <div name="chat-container">
          {
            this.state.chat.isOpened && <ChatApp username={this.state.chat.name}/>
          }
          </div>
        </div>
          
      </div>
    );
  }
}
