import React, { Component } from 'react';
import {Logo} from './Logo.js';
import LoginButton from './LoginButton.js';
import './Login.css';

export default class Login extends React.Component {
     render() {
         return (
            <div>
                <img className='Background'/>
                <Logo />
                <LoginButton loginCallback={this.props.loginCallback} />
            </div>
        );
     }
     
}