import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fab } from '@fortawesome/free-brands-svg-icons'
import FacebookLoginButton from './FacebookLoginButton';

export default class LoginButton extends React.Component {

    constructor() {
      super();
      this.state = {
        username: null
      };
    }

      onFacebookLogin = (loginStatus, resultObject) => {
        if (loginStatus === true) {
          this.setState({
            username: resultObject.user.name
          });
          this.props.loginCallback();
        } else {
          alert('Facebook login error');
        }
      }
    
     render() {
       library.add(fab);
       const { username } = this.state;
       
       return (
          <div class="centerr">
          {!username &&
            <div>
             <FacebookLoginButton style={{margin: "2em auto"}} onLogin={this.onFacebookLogin}>
                <Button variant="contained"
                    color='primary'
                    size='large'
                    onClick={this.facebookLogin}>
                    <FontAwesomeIcon name='facelogo' icon={['fab', 'facebook']} className="faClass"/>
                    Login with Facebook
                </Button>
              </FacebookLoginButton> 
             </div>
            } 
           {username &&
            <p>Welcome, {username}</p>
            } 
          </div>
        );
    }
}