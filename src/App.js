import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { NoOneAround } from './components/no-one'
import { HamburgerButton } from './components/hamburger'
import { PersonCard } from './components/person-card'
import ChatApp from './components/chat-app/ChatApp'
import Login from './components/login/Login'
import FindingPage from './components/findingPage'

var rp = require('request-promise');

class App extends Component {

  constructor() {
    super()
    
    this.state = {
      isLoggedin: false
    }

    this.loginCallback = this.loginCallback.bind(this);
  }

  loginCallback() {
    console.log("logged in !!!")
    this.setState({
      isLoggedin: true
    })
  }

  render() {
    console.log("stateee", this.state);
    if (!this.state.isLoggedin) {
      return <Login loginCallback={this.loginCallback}/>;
    } else {
      return <FindingPage/>
    }
  }
}

export default App;
